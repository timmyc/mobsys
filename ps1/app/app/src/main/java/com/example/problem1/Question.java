package com.example.problem1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Question {

    private int questionId;
    private boolean trueAnswer;
}

